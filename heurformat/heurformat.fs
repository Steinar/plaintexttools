#! /usr/bin/env gforth

\ Heuristic formatter. Re-flow lines with a non-space character in leftmost
\ column to terminal width, leave empty and indented lines as is.

: print-msg-and-exit
    ['] type stderr outfile-execute ['] cr stderr outfile-execute 1 (bye) ;

: print-msg-and-exit?
    rot if print-msg-and-exit else 2drop then ;

s" COLUMNS" getenv dup 0= s" Need $COLUMNS" print-msg-and-exit?
s>number? invert s" Unexpected $COLUMNS" print-msg-and-exit?
d>s value LINELENGTH

LINELENGTH 20 < s" Unexpected value of $COLUMNS" print-msg-and-exit?

LINELENGTH 2* value MAX-READ-LENGTH
MAX-READ-LENGTH 2 + value BUFFERLENGTH
0 value linebuffer
variable current-linewidth

\ TODO split too large words into smaller
\ TODO doesn't feel like Forth, the code is clumsy

: full-line? ( u1 offset -- bool )
    MAX-READ-LENGTH swap - < ;

: read-stdin ( offset -- u2 not-eof )
    locals| offset |
    linebuffer offset + MAX-READ-LENGTH offset - stdin read-line
    s" Error reading from STDIN." print-msg-and-exit? ;

: resize-buffer ( -- )
    linebuffer BUFFERLENGTH 2* resize
    0<> s" Could not resize line buffer." print-msg-and-exit?
    to linebuffer
    BUFFERLENGTH 2* to BUFFERLENGTH
    BUFFERLENGTH 2 - to MAX-READ-LENGTH ;

: read-full-line ( -- u not-eof )
    0 0 locals| offset not-eof |
    begin
        offset read-stdin
        to not-eof
        offset
        2dup + to offset
        full-line? 0=
    while
        resize-buffer
    repeat
    offset not-eof ;

: allocate-buffer ( u -- )
    allocate 0<> s" Could not allocate I/O buffer." print-msg-and-exit?
    to linebuffer ;

: find-space ( c-addr u -- x ) \ if x < 0, no space found, else the offset
    ?dup 0= if drop -1 exit then
    0 do
        dup i + c@ bl = if
            drop
            i unloop exit
        then
    loop
    drop
    -1 ;

: next-linewidth ( wordwidth -- nextwidth )
    locals| wordwidth |
    current-linewidth @ dup 0<> if
        1+
    then
    wordwidth + ;

\ I first assumed I could use x-width, but that was wrong, also my assumption
\ which followed, that xchars are UCS-n in memory, is also wrong. I gave up and
\ used my own code:
: utf8-width ( wordstart len -- n )
    \ really naïve, but what the hey...
    \ assumes well-formed input without decomposed characters or "fancy" stuff
    0 -rot
    0 ?do
        dup >r c@
        dup 192 and 192 = if
            drop 1+
        else
            128 and 0= if
                1+
            then
        then
        r> 1+
    loop
    drop ;
        
: write-single-word ( wordstart len -- )
    0 0 locals| wordwidth nextwidth len wordstart |
    wordstart len utf8-width to wordwidth
    wordwidth next-linewidth to nextwidth
    nextwidth LINELENGTH <= if
        current-linewidth @ 0> if space then
        nextwidth current-linewidth !
    else
        current-linewidth @ 0> if cr then
        wordwidth current-linewidth !
    then
    wordstart len type ;

: write-words-in-line ( maxlen -- )
    0 0 locals| runningoffset found maxlen |
    begin
        linebuffer runningoffset + maxlen runningoffset - find-space
        dup 0>=
    while
        ?dup 0= if
            runningoffset 1+ to runningoffset
        else
            to found
            linebuffer runningoffset + found write-single-word
            runningoffset found + 1+ to runningoffset
        then
    repeat
    drop
    runningoffset maxlen < if
        linebuffer runningoffset + maxlen runningoffset - write-single-word
    then ;

: dump-verbatim ( u -- )
    linebuffer swap type cr ;

: indented? ( -- ) linebuffer c@ bl = ;

: end-dangling-paragraph ( -- )
    current-linewidth @ 0> if cr then
    0 current-linewidth ! ;

: process-non-empty ( u -- )
    indented? if
        end-dangling-paragraph
        dump-verbatim
    else
        write-words-in-line
    then ;

: process-line ( u -- )
    ?dup 0= if
        end-dangling-paragraph
        cr
    else
        process-non-empty
    then ;

: main
    BUFFERLENGTH allocate-buffer
    0 current-linewidth !
    begin
        read-full-line
    while
        process-line
    repeat
    0<> s" Last read length was not 0." print-msg-and-exit?
    current-linewidth @ 0<> if cr then
    linebuffer free drop ;

main
bye
