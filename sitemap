#! /usr/bin/env python

import getopt, os, os.path, re, sys

HELP_TEXT = """\
sitemap [-h]

-h
    Print this text and exit.

Recursively search for all files under the current directory named "README"
(with fallback to similar names) and heuristically generate summaries, dumping
to stdout."""

INDEX_LINE_PATTERN = re.compile(r"\d+\.\d+ \S.*\s\d+")
README_FILENAMES = ("README", "README.txt", "about.txt")


def find_readme():
    to_index = []
    for root, dirs, files in os.walk(os.path.curdir):
        for filename in README_FILENAMES:
            if filename in files:
                to_index.append(os.path.join(root, filename))
                break
    return to_index

def is_index(paragraph):
    for line in paragraph:
        if None == INDEX_LINE_PATTERN.fullmatch(line.strip()):
            return False
    return True

def title_and_first_paragraph(readmepath):
    paragraphs = []
    current_paragraph = []
    for line in open(readmepath, "r"):
        if len(line.strip()) == 0:
            if len(current_paragraph) > 0:
                paragraphs.append(current_paragraph)
                current_paragraph = []
                if len(paragraphs) == 3:
                    break
        else:
            current_paragraph.append(line)
    if len(current_paragraph) > 0:
        paragraphs.append(current_paragraph)
    if len(paragraphs) == 0:
        return ["\n"]
    summary = []
    summary.extend(paragraphs[0])
    if len(paragraphs) > 2 and is_index(paragraphs[1]):
        summary.append("\n")
        summary.extend(paragraphs[2])
    elif len(paragraphs) > 1:
        summary.append("\n")
        summary.extend(paragraphs[1])
    return summary

def present(readmepath, outstream):
    outstream.write(readmepath)
    outstream.write("\n")
    outstream.writelines(title_and_first_paragraph(readmepath))

def sitemap(outstream):
    readmes = find_readme()
    readmes.sort()
    first = True
    for readme in readmes:
        if first:
            first = False
        else:
            outstream.write("\n\n")
        present(readme, outstream)

def main():
    options, arguments = getopt.gnu_getopt(sys.argv[1:], "h")
    for name, value in options:
        if name == "-h":
            print(HELP_TEXT)
            sys.exit(0)
    if len(arguments) > 0:
        print(HELP_TEXT)
        sys.exit(1)
    sitemap(sys.stdout)

if __name__ == "__main__":
    main()
